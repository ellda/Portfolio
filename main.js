/******************************************************************************
 *	File : main.js                                                            *
 *  Domain : www.antoinefonfria.com                                           *
 *  Author : Antoine Fonfria                                                  *
 ******************************************************************************/

var body = $('body'),
	panel = $('#panel'),
	panel_content = $('#panel_content'),
	_state,
	_active_section,
	panelTimeout;

var wWin = $(window).width(),
	hWin = $(window).height(),
	wBG = 4800,
	hBG = 3300,
	xBG = (wWin - wBG) / 2,
	yBG = (hWin - hBG) / 2,
	xBG_temp = xBG,
	yBG_temp = yBG,
	xDist = 0,
	yDist = -500;

/** Floating object coords
 * - size : { width px , height px },
 * - init : { left %, top % },
 * - focus : { top px},
 * - wait : { left_position boolean, relative_position int }
 */
var object_coords = {
	character : {
		size : { w: 500, h:500 },
		init : { x: 50, y: 50 },
		focus : { top : -20},
		wait : { left: true, pos: 0 }
	},
	school : {
		size : { w: 300, h:300 },
		init : { x: 30, y: 30 },
		focus : { top : 0},
		wait : { left: true, pos:4 }
	},
	internships : {
		size : { w: 300, h:300 },
		init : { x: 25, y: 65 },
		focus : { top : 0},
		wait : { left: true, pos:6 }
	},
	elldabox : {
		size : { w: 250, h:220 },
		init : { x:72, y: 28 },
		focus : { top : 0},
		wait : { left: false, pos:2 }
	},
	leadprojects : {
		size : { w: 250, h:220 },
		init : { x:80, y: 46 },
		focus : { top : 0},
		wait : { left: false, pos:4 }
	},
	biprojects : {
		size : { w: 250, h:220 },
		init : { x:75, y: 67 },
		focus : { top : 0},
		wait : { left: false, pos:6 }
	}
}


/******************************************************************************
 *                                  ON LAUNCH                                 *
 ******************************************************************************/

$(document).ready(function(){

	/* Background animation */
	body.css('background-position', (xBG)+'px '+(yBG)+'px');
	if(window.requestAnimationFrame)
		requestAnimationFrame(BGAnimation);

	/* Initiate tooltip positions */
	tooltipPositions_init();

	/* Initiate islands positions */
	islandPosition_init($('.anchor'));
	_state = 0;

	/* Open panel click handler */
	$(body).on('click', '.anchor.clickable', function(){
		/* Vars */
		_state = 1;
		_active_section = $(this).find('.island').attr('id');
		/* Islands */
		$('.anchor').stop().removeClass('moved');
		$(this).addClass('moved');
		islandPosition_wait($('.anchor').not('.moved'));
		islandPosition_focus($(this));
		/* Panel */
		displayPanel($(this));
		displayInformations();
	});

	/* Close panel click handler */
	$(body).on('click', '#background, .panel_corner.upright', function(){
		$('.anchor').stop().removeClass('moved');
		islandPosition_init($('.anchor'));
		hidePanel();
		_state = 0;
		_active_section = null;
	});

	/* ScrollBar */
	$('.customScroll').perfectScrollbar();

});


/******************************************************************************
 *                                  ON RESIZE                                 *
 ******************************************************************************/

$(window).resize(function() {
	wWin = $(window).width();
	hWin = $(window).height();
	if(_state == 1){
		islandPosition_wait($('.anchor').not('.moved'));
		islandPosition_focus($('.anchor.moved'));
	}
});

/******************************************************************************
 *                               INIT FUNCTIONS                               *
 ******************************************************************************/

/* TOOLTIPS INIT */
function tooltipPositions_init(){
	$('.tltpAnc').find('.tooltip').each(function(){
		var left = - ($(this).width() / 2 + 10),
			leftFromRightWindow = - ($(this).parent().offset().left - (wWin - $(this).width() - 35 ));
		$(this).css('left', Math.min(left, leftFromRightWindow) + 'px');
	});
}


/******************************************************************************
 *                             ISLANDS POSITIONS                              *
 ******************************************************************************/

/* Initial position */
function islandPosition_init(anchors){
	for(var i = 0; i < anchors.length; i++){
		anchors.eq(i).css('z-index', '50')
			.addClass('clickable')
			.animate({
				left: object_coords[anchors.eq(i).find('.island').attr('id')].init.x+'%',
				top: object_coords[anchors.eq(i).find('.island').attr('id')].init.y+'%'
			}, 1000, "easeOutQuart");
	}
}

/* On focus position */
function islandPosition_focus(anchor){
	anchor.css('z-index', '100')
		.removeClass('clickable')
		.stop().animate({
			left: (wWin/2)+'px',
			top: object_coords[_active_section].size.h/2+(object_coords[_active_section].focus.top)+'px'
		}, 1000, "easeOutQuart");
}

/* Wait position */
function islandPosition_wait(anchors){
	var data;
	for(var i = 0; i < anchors.length; i++){
		c = object_coords[anchors.eq(i).find('.island').attr('id')];
		anchors.eq(i).css('z-index', (c.wait.pos+50)).addClass('clickable');
		if(c.wait.left)
			anchors.eq(i).stop().animate({
				left: (c.size.w/2+10)+'px',
				top: ((c.size.h/2)+(c.wait.pos*10*(hWin/100)))+'px'
			}, 1000, "easeOutQuart");
		else
			anchors.eq(i).stop().animate({
				left: wWin-(c.size.w/2+10)+'px',
				top: ((c.size.h/2)+(c.wait.pos*10*(hWin/100)))+'px'
			}, 1000, "easeOutQuart");
	}
}


/******************************************************************************
 *                                INFO PANEL                                  *
 ******************************************************************************/

function displayPanel(anchor){
	panel.stop()
		.css('z-index', '110')
		.animate({
			width: '60%',
			height: '60%',
			left: '20%',
			top: '25%',
			opacity:'1'
		}, 1000, "easeOutQuart");
}

function hidePanel(){
	clearTimeout(panelTimeout);
	panel_content.stop(true).scrollTop(0).css('display', 'none');
	panel.stop()
		.css('z-index', '10')
		.animate({
			left: '49%', top: '49%', height: '2%', width: '2%',
			opacity:'0'
		}, 1000, "easeOutQuart");
}

function displayInformations(){
	var delay = 800;
	if(panel_content.css('display') == 'block'){
		panel_content.stop().fadeOut(500);
		delay = 500;
	}

	panelTimeout = setTimeout(function(){

		var secData, secContent, pClass, headers, gallery;

		panel_content.html('<h1>'+object_data[_active_section].title+'</h1>');
		if(object_data[_active_section].intro != undefined)
			panel_content.append('<div class="introduction">'+object_data[_active_section].intro+'</div>');

		for(var sec in object_data[_active_section].textSections){

			secData = object_data[_active_section].textSections[sec];
			secContent = '<div class="panel_content_section">';

			// Title
			if(secData.title != undefined)
				secContent += '<h2>'+secData.title+'</h2>';
			// Tags
			if(secData.tags != undefined){
				var tags = '';
				for (var t in secData.tags)
					tags += '#'+secData.tags[t]+' ';
				secContent += '<div class="tags">'+secData.tags.join(', ')+'</div>';
			}
			// Logo
			pClass = "";
			if(secData.logo != undefined)
				secContent += '<div class="section_logo"><img title="'+secData.logo+'" src="img/logos/'+secData.logo+'.png" /></div>';
			else
					pClass = 'class="indented"';
			// Content Headers
			if(secData.contentHeaders != undefined){
				headers = '<h4>';
				for(var h in secData.contentHeaders)
					headers += '<span class="bold">' + secData.contentHeaders[h].label + ' :</span> ' + secData.contentHeaders[h].value + '<br />';
				secContent += headers + '</h4>';
			}
			// Content & Gallery
			for(var c in secData.content){
				if(secData.content[c] == "GALLERY"){
					gallery = '<div class="gallery">';
					for(var g in secData.gallery){
						gallery += '<img src="img/galleries/' + secData.gallery[g] + '.png" data-pos="'+ g +'" />';
						if(_active_section == "elldabox")
							gallery += '<br /><br /><br />';
					}
					gallery += '</div>';
					secContent += gallery;
				}
				else
					secContent += '<p '+ pClass +'>'+secData.content[c]+'</p>';
			}
			// Links
			if(secData.links != undefined){
				secContent += '<div class="links">';
				for(var l in secData.links){
					secContent += '<a target="_blank" href="'+secData.links[l].link+'">'+secData.links[l].title+'<img src="img/link.png" /></a> ';
				}
				secContent += '</div>';
			}

			secContent += '</div>';
			panel_content.append(secContent);
		}

		//panel_content.append('<div class="panel_content_footer"></div>');
		panel_content.fadeIn(500);

	}, delay);

}


/******************************************************************************
 *                               GALLERY SLIDER                               *
 ******************************************************************************/

$('#panel_content').on('click', '.gallery img', function(){

	var images = $(this).closest('.gallery').find('img'),
		currentImgSrc = $(this).attr('src'),
		currentImgPos = $(this).data('pos'),
		srcs = [];

	$.each(images, function(){
		srcs[$(this).data('pos')] = $(this).attr('src');
	});

	var imgEl = $('<img />').attr('src', currentImgSrc),
		switcherLeft = $('<div />').addClass('switcher left'),
		switcherRight = $('<div />').addClass('switcher right');

	var imgcont = $('<div />').addClass('imagecontainer').append(switcherLeft, imgEl, switcherRight),
		midtable = $('<div />').addClass('midtable').append(imgcont);

	var newDiv = $('<div />').addClass('fullsizeimagebox').append(midtable).on('click', function(){
		$(this).fadeOut(300, function(){
			$(this).remove();
		});
	}).on('click', '.switcher', function(e){
		e.stopPropagation();
	}).appendTo($('body')).hide().fadeIn(300);

	imgEl.css('max-width', (wWin - 120)+'px');
	imgEl.css('max-height', (hWin - 100)+'px');

	imgEl.on('load', function(){
		$('.switcher').css('height', imgEl.height() + 10 + 'px');
		if(srcs.length == 1)
			$('.switcher').css('width', '0');
	});

	$('.switcher').on('click', function(){

		($(this).hasClass('left')) ? currentImgPos-- : currentImgPos++;

		if(currentImgPos < 0)	currentImgPos = srcs.length-1;
		if(currentImgPos > srcs.length-1)	currentImgPos = 0;

		$(this).closest('.midtable').find('img').attr('src', srcs[currentImgPos]);

		/*$(this).closest('.midtable').find('img').fadeOut(400, function() {
				$(this).attr('src', srcs[currentImgPos]);
				$(this).fadeIn(400);
		});*/
	});


});


/******************************************************************************
 *                           BACKGROUND ANIMATION                             *
 ******************************************************************************/

$(document).mousemove(function(e){

	xDist = e.clientX - wWin/2;
	yDist = e.clientY - hWin/2;

});

function BGAnimation() {
	// Recalculate the BG coordinates with easing effect
	xBG_temp = easeAnimation(xBG_temp, xBG-xDist/2, 100, 0.1);
	yBG_temp = easeAnimation(yBG_temp, yBG-yDist/2, 100, 0.1);

	body.css('background-position', xBG_temp+'px '+yBG_temp+'px');
	requestAnimationFrame(BGAnimation);
}

// Returns the n value to make it closer to goal, according to a factor and a limit
// Credits : BaliBalo
function easeAnimation(n, goal, factor, limit)
{
	return (limit && Math.abs(goal - n) < limit) ? goal : n + (goal - n) / (factor || 10);
};
