/******************************************************************************
 *	File : data.js                                                            *
 *  Domain : www.antoinefonfria.com                                           *
 *  Author : Antoine Fonfria                                                  *
 ******************************************************************************/

var object_data = {
	character : {
		title : "Bienvenue !",
		textSections : [
			{
				content : ["Je m'appelle Antoine Fonfria, j'ai 24 ans et je suis étudiant ingénieur à l'école Polytech Tours (37) en spécialité informatique. Dans ce portfolio, vous trouverez dans les deux premières sections des informations sur mon parcours scolaire et professionnel, puis certains de mes projets avec pour chacun une description du contexte et de leur principe. Ce site a été entèrement réalisé par mes soins à l'aide de logiciels libres (Atom, GIMP) et de quelques plugins javaScript (jQuery, perfectScrollbar). Le fond étoilé a été travaillé à partir d'une photographie. Le résultat reflète tant ma passion pour les technologies du web et l'informatique en général que pour le dessin et le graphisme. N'hésitez pas à me contacter grâce à la petite enveloppe dans le coin supérieur droit pour toute question ou remarque. Je vous souhaite une agréable navigation dans ce petit espace d'internet."]
			},{
				title : "A propos de moi",
				logo : "hobbies",
				content : ["D'un naturel curieux, je voue beaucoup d'intérêt à l'informatique et à la technologie, mais aussi aux sciences sociales, à la médecine, la physique quantique et l'astronomie. J'aime me tenir au courant de l'actualité scientifique, que ce soit grâce à la lecture de revues généralistes ou en parcourant quotidiennement différents articles sur internet. Le cinéma, la musique, les séries TV et les jeux-vidéos sont également mes sources de divertissement et d'inspiration privilégiées. Je pratique de plus le dessin et la guitare à mes heures perdues.",
				"Multipliant les centres d'intérêts culturels et intellectuels, c'est après plusieurs essais dans d'autres domaines que j'ai finalement orienté mon parcours en choisissant une formation dans l'informatique. Associant le plaisir du jeu vidéo à celui de la création et tout en restant à l'affut des dernières inventions et innovations, j'ai pu m'exprimer dans de nombreux projets passionnants. Grâce à ces derniers, j'ai su acquérir des bases solides et polyvalentes que j'aspire à développer dans un cadre plus professionnel, tout en mettant à profit mon esprit de synthèse et d'analyse."]
			}
		]
	},
	school : {
		title : "Mon cursus",
		textSections : [
			{
				content : ["Intégré en 2011 sur concours, le parcours préparatoire de l'école Polytech Tours (PeiP) m'a permis de suivre pendant deux ans une formation généraliste partagée avec la licence de mathématiques à l'Université François Rabelais. Grâce à celui-ci, j'ai pu acquérir de bonnes bases en algèbre et analyse tout en confortant mon choix de formation grâce aux divers mini-projets et options. Polytech formant un réseau d'écoles à travers la France, j'ai par la suite été accepté en spécialité informatique à Tours, cursus d'ingénierie en 3 ans. Actuellement en dernière année, l'école m'a donné l'occasion d'aborder de nombreuses compétences dans une multitude de domaines et de langages, du développement mobile au clustering de serveurs, en passant par la stratégie d'entreprise, l'ingénierie logicielle ou encore l'algorithmique avancé. Trois stages (1ère, 4ème et 5ème année), une expérience à l'international et une certification de niveau d'anglais (score personnel au TOEIC de 950) viennent parfaire cette formation."],
				logo : 'polytech',
				title : 'Polytech Tours',
				links : [
					{title:"Réseau Polytech", link:"http://www.polytech-reseau.org"},
					{title:"Polytech Tours", link:"http://polytech.univ-tours.fr"},
					{title:"Université François Rabelais", link:"http://www.univ-tours.fr"}]
			},{
				title : "Université de Sherbrooke (QC)",
				content : ["Au cours de ma 4ème année (2014), j'ai choisi de réaliser le semestre d'automne au Canada. Validant ainsi mon expérience à l'étranger, j'ai suivi les cours de maîtrise en informatique de l'Université de Sherbrooke, au Québec. A cette occasion, j'ai acquis de nouvelles compétences en ingénierie logicielle, gestion de projet, Java, algorithmique, ainsi qu'une approche théorique des interfaces utilisateurs. Profitant d'une immersion prolongée d'environ 5 mois, j'ai également beaucoup voyagé, découvrant les paysages incroyables de cette région du monde. J'ai aussi pu mettre en pratique mon anglais au cours d'une excursion à Toronto et aux chutes du Niagara, et de la célébration du nouvel an 2015 sur Times Square, en plein coeur de New York."],
				logo : 'universitedesherbrooke',
				links : [
					{title:"Université de Sherbrooke", link:"http://www.usherbrooke.ca/"}]
			}
		]
	},
	internships : {
		title : "Mes stages",
		textSections : [
			{
				title : 'Beemotion',
				logo : 'beemotion',
				contentHeaders : [
					{label : "Poste", value : "Développeur web assistant ingénieur"},
					{label : "Durée", value : "3 mois"}
				],
				content : ["Beemotion est une entreprise installée à la pépinière Start'Inbox de Tours spécialisée dans le transport public de voyageurs. Elle propose différents services et prestations aux communautés d'agglomération et opérateurs de transports. Son expertise repose sur une analyse approfondie des données récupérées lors de l'exploitation de bus et tramways afin d'en optimiser leur réseau. C'est dans ce contexte que j'ai participé au développement d'une application web de Main Courante, permettant aux opérateurs d'un réseau de transport de voyageurs de reporter et de consigner les évènements imprévus quotidiennement.",
				"GALLERY",
				"Travaillant en équipe avec M. Portier, directeur associé, et M. Tissier, également stagiaire, j'ai principalement créé l'interface de paramétrage (partie Front-End). Elle permet une configuration très poussée de l'application et intègre, en JavaScript, un système orienté-objet avec pseudo-classes grâce à jQuery. De nombreuses communications client-serveur (Ajax) en Cross-Domain sont aussi pris en charge, avec une attention particulière portée sur la vérification des données utilisateurs et la sauvegarde du travail (messages d'alertes, mises en évidence des éléments modifiés, etc)."],
				links : [
					{ title : "Beemotion", link : "http://www.beemotion.eu/"}
				],
				gallery : ["beemotion"]
			},{
				title : 'Exitis',
				logo : 'exitis',
				contentHeaders : [
					{label : "Poste", value : "Développeur web"},
					{label : "Durée", value : "2 x 1 mois"}
				],
				content : ["Également basé à Tours, Exitis est un bureau d'études spécialisé dans la gestion et la prévention du risque incendie. Faisant le lien entre les services de sécurité, la mairie et les propriétaires de sites (supermarchés, immeubles de grande hauteur, ...), ils fournissent entre autres choses une expertise avisée en matière de réglementation sur les équipements de sécurité et leurs maintenances.",
				"Durant mon stage, j'ai tenté de définir une solution informatique en remplacement d'un système non automatisé, en étroite collaboration avec le directeur, M. Hummel Jean-François. Satisfait de mon travail, il m'a invité à le poursuivre l'année suivante sous la forme d'un contrat rémunéré d'un mois (CDD). Suite à ces deux expériences, le résultat prit la forme d'un tableau de bord dynamique permettant aux employés de la société de suivre l'état des équipements présents sur les sites surveillés. Une partie \"ressources\" autorise la manipulation des données de l'application (types d'équipements, références légales, types de sites, ...). Les paramètres sont ensuite pris en compte pour afficher des alertes lorsqu'une vérification arrive à échéance par exemple. L'application web à l'accès sécurisé inclut de nombreux formulaires dynamiques (Ajax) pour que l'équipe puisse mettre à jour les informations. Elle comprend de plus une vue d'impression facilitant l'exportation de documents, tels qu'une fiche associée à un site en particulier."],
				links : [
					{ title : "Exitis", link : "http://www.exitis.fr/"}
				]
			}
		]
	},
	elldabox : {
		title : "Elldabox",
		intro : "Développement d'un site sur plusieurs années grâce à un apprentissage autodidacte dans un cadre purement personnel.",
		textSections : [
			{
				title : 'Génèse',
				logo : 'elldabox',
				content : ["Elldabox est un projet qui est né en 2010 lors de ma première année à l'université à l'occasion d'un cours d'initiation au développement de sites internet. D'abord le résultat d'un exercice, c'est devenu un blog, puis un espace de discussion et enfin un simili de réseau social. Aujourd'hui, cet espace privé est le refuge d'une petite communauté d'amis : nous partageons ce que nous trouvons sur le web, discutons de nos centres d'intérêts communs et planifions nos divers évènements. Il intègre de nombreuses fonctionnalités venues s'ajouter au fil de mises à jours plus ou moins régulières, selon mes idées et les retours de mes utilisateurs."]
			},{
				title : 'Développement et compétences',
				tags : ['HTML/CSS', 'JavaScript', 'PHP', 'PDO', 'MySQL', 'PhpMyAdmin', 'Webmastering'],
				content : ["Réalisé durant mon temps libre, j'ai fait le pari de développer chaque fonctionnalité à la main. Cela m'a permit d'apprendre les langages du web de manère autonome : HTML, CSS, JavaScript (avec jQuery), mais aussi PHP et MySQL avec PDO. J'ai porté le projet instensément durant 3 années, le laissant depuis 2013 en relative stagnation, préférant me concentrer sur mes autres projets et ma formation. J'y suis toutefois toujours très attaché et grâce à la motivation de l'ensemble de mes utilisateurs, je continue de l'administrer et d'y organiser différents évènements."]
			},{
				title : 'Fonctionnalités',
				content : [
					"GALLERY",
					"Cette section présente une liste non-exhaustive des fonctionnalités notables de mon site. Elles ont pour la plupart fait l'objet de nombreuses phases de conceptions, de tests et de refontes avant d'obtenir le résultat actuel. Chaque fois, je suis parti d'une idée de base, puis j'ai fais une synthèse de ce qui existait déjà et pris en compte les remarques de mes utilisateurs une fois les premières maquettes réalisées.",
					"<span class=\"bold\">Page de partage (Box) :</span> Première section majeure, elle permet d'insérer une vidéo, une image ou un lien depuis une url. Celle-ci est interprétée automatiquement et le site génère le contenu. A l'objet publié s'ajoute un système de commentaires et de \"votes\" (pouces verts ou rouges). Une recherche par mot clé, par favoris ou par mot-clés permet de retrouver les publications.",
					"<span class=\"bold\">Forum de discussion :</span> Seconde section majeure, le forum comprends des sujets eux-mêmes composés de messages. Ces topics sont triés par ordre anté-chronologique de derniers messages et regroupés en catégories propres à nos centres d'intérêts en communs.",
					"<span class=\"bold\">News et sondages :</span> Une page d'accueil me permet d'afficher des nouvelles importantes concernant le site, ainsi que de réaliser divers sondages souvent propices aux jeux et évènements.",
					"<span class=\"bold\">Messagerie instantannée :</span> Utilisant AJAX, cette messagerie groupée sous la forme d'un onglet en bas de la page est accessible en permanence sur tout le site et permet de voir les membres en ligne.",
					"<span class=\"bold\">BBCode :</span> Chaque message (commentaire, forum) posté par un utilisateur peut contenir des balises spéciales permettant d'ajouter des émoticônes, de modifier le style (couleur, taille, gras, ...), ou d'insérer des éléments (vidéo, image, lien ou bloc de texte centré, limité au forum).",
					"<span class=\"bold\">Niveaux et Personnalisation :</span> Le compte d'un utilisateur progresse en niveau à chaque action sociale sur le site (publication, message, vote). De plus, chaque membre peut personnaliser la couleur de son nom et la couleur principale du site. Le design est adapté en conséquence. Enfin, des titres qui peuvent être apposés à côté du pseudonyme sont à gagner régulièrement au travers de concours ou à l'occasion d'un haut-fait particulier. Chacun a donc son histoire : Eleveur de chat, Ingénieur, Le 1000ème, Demi-princesse, ... et j'en passe.",
					"<span class=\"bold\">Classements et statistiques :</span> Les utilisateurs peuvent se comparer entre eux au travers de nombreux scores (nombre de \"pouces verts\", nombre de messages, ...) et ils sont automatiquement classés selon leur niveau.",
					"<span class=\"bold\">Notifications :</span> Regroupées en 3 catégories (Box, Forum, Social), les notifications sont entièrement dynamiques et permettent d'avertir l'utilisateur d'un nouvel évènement. Ce peut être la publication d'un article, une réponse à un forum ou la montée en niveau d'un autre membre."],
				gallery : ["elldabox1", "elldabox2", "elldabox3", "elldabox4", "elldabox5"]
			}
		]
	},
	leadprojects : {
		title : "Projets de groupe",
		intro : "Chef d'équipe sur deux semestres lors de ma 4ème année d'études.",
		textSections : [
			{
				title : 'Outil collaboratif pour les enfants atteints de TSA',
				logo : 'tichef',
				tags : ['HTML/CSS', 'JavaScript', 'PhoneGap', 'iPad', 'Graphisme', 'Management'],
				contentHeaders : [
					{label : "Choix du sujet", value : "Sélection à partir d'une liste"},
					{label : "Contraintes", value : "Mise en oeuvre d'une méthode de génie logiciel, relationnel client"},
					{label : "Délai", value : "4 mois, 4h par semaine"}
				],
				content : ["En tant que chef de projet d'une équipe de 8 étudiants de Polytech, j'ai pour la seconde fois mis en pratique la méthode agile XP à l'occasion d'un Projet d'Ingénierie Logiciel. En partenariat avec l'équipe de médecins et d'infirmières du pôle Autisme du CHU Bretonneau de Tours, nous avons réalisé une application sur iPad destinée à promouvoir les interactions sociales entre enfants atteints de TSA (Troubles du Spectre Autistiques). Au fil des réunions, nous avons dégagé l'idée d'un livre de recettes de cuisine interactif, faisant la liaison avec une activité manuelle pré-existante au sein du service.",
				"GALLERY",
				"A terme, nous présentions une application web épurée et adaptée au support grâce à PhoneGap. Elle intègre donc un suivi de recette avec différentes étapes (sélection des ingrédients et ustensiles, mélange, cuisson, etc.) et également la possibilité de prendre des photos à tout moment de la préparation. Une page protégée par mot de passe permet de gérer les recettes existantes, mais aussi les profils associés aux enfants. Ceux-ci permettent de conserver l'historique des activités, l'enfant pouvant faire défiler les photos et noter son appréciation globale. De plus, ils intègrent un système de paramétrisation (présence ou non de texte, photographies ou pictogrammes pour les objets). Ces diverses fonctionnalités, ainsi que la charte graphique, s'inscrivent dans une recherche active avec le personnel soignant pour répondre au mieux aux besoins des enfants atteints de cette pathologie. En plus de la gestion du groupe et du codage de certaines fonctions, j'ai réalisé le logo et conçu la majorité du design de l'application en orientant mon travail vers une ergonomie adaptée au support mobile."],
				links : [
					{title:"Article sur le site de l'AESPHOR", link:"http://www.aesphor-c.org/1/espace_numerique_876575.html"}],
				gallery : ["tc1", "tc2"]
			},
			{
				title : 'Jeu Casse-briques',
				logo : "cassebriques",
				tags : ['Java', 'Design Patterns', 'SVN', 'Redmine', 'Agile XP', 'Management'],
				contentHeaders : [
					{label : "Choix du sujet", value : "Libre"},
					{label : "Contraintes", value : "Langage Java, design patterns, méthode agile XP"},
					{label : "Délai", value : "3 mois, 10h par personne"}
				],
				content : ["GALLERY",
				"C'est durant ma session au Québec que j'ai eu l'occasion de diriger une équipe de 12 étudiants dans le cadre d'un cours sur le Java. Nous devions proposer un sujet et mettre en pratique plusieurs compétences : la méthode Agile eXtreme Programming, les patrons de conception et la gestion d'un projet en groupe (rôles, réunions, SVN et Redmine, communication, ...). Me portant volontaire pour le rôle de chef, j'ai pu découvrir certains aspects de cette fonction : attribuer les tâches, animer les réunions, gérer les conflits, prendre des décisions parfois difficiles... Sans oublier ma part du développement et de revue de code.",
				"Notre application, téléchargeable ci-dessous, propose une version infinie du jeu de casse-brique : une barre est contrôlée horizontalement par les touches du clavier et fait rebondir une balle à son contact. Celle-ci détruit des briques situées en haut de l'écran, ce qui permet d'engranger des points. Des briques apparaissent régulièrement et la partie est terminée lorsque la balle touche le bord inférieur trois fois de suite."],
				links : [
					{title:"Télécharger le jeu", link:"http://www.antoinefonfria.com/demo/CasseBriques.zip"}],
				gallery : ["cb"]
			}
		]
	},
	biprojects : {
		title : "Autres projets",
		intro : "Quelques applications réalisées en binôme tout au long de mes études.",
		textSections : [
			{
				title : 'Jeu Snake multijoueurs',
				logo : 'snakewars',
				tags : ['C', 'Génie Logiciel', 'Graphisme', 'Gimp'],
				contentHeaders : [
					{label : "Choix du sujet", value : "Proposition optionnelle"},
					{label : "Contraintes", value : "Langage C, Algorithmes avancés"},
					{label : "Délai", value : "4 mois, 4h par semaine"}
				],
				content : ["Snake Wars est le fruit d'un exercice de développement en C-89 au cours duquel nous avons pu proposer notre sujet. Mon binôme (M. Fauque Benoît) et moi-même étions enthousiastes à l'idée de créer un jeu tout en mettant en pratique volontairement une méthode du Génie Logiciel vue en cours. Nous avons donc tenté d'appliquer un cycle en V dans le temps imparti. A terme, nous avons fait de la conception et rédigé de la documentation les 3 premiers quarts avant un sprint de développement final.",
				"GALLERY",
				"Notre jeu s'articule autour des principes suivants : les joueurs (1 à 4) composés d'humain(s) et/ou d'ordinateur(s) s'affrontent sur un terrain limité où l'objectif est d'amasser le plus de point tout en survivant le plus longtemps. Chaque joueur contrôle un serpent, qui avance en permanence et s'allonge chaque fois qu'il mange un fruit. Les fruits donnent des points (5 types de fruits) et apparaissent à intervalle régulier sur des endroits aléatoires du terrain. Un joueur perd lorsque la tête de son serpent vient heurter un obstacle (sa propre queue ou un serpent adverse). Un écran des scores apparaît lorsque tous les joueurs ont été éliminés. Au cours du développement, j'ai notamment réalisé toute l'interface graphique sous GIMP, la gestion du temps-réel et le comportement pseudo-aléatoire de l'ordinateur."],
				links : [
					{title:"Télécharger (Windows)", link:"http://www.antoinefonfria.com/demo/SnakeWars_win64.zip"},
					{title:"Télécharger (Linux)", link:"http://www.antoinefonfria.com/demo/SnakeWars_linux64.zip"},
					{title:"Benoit Fauque", link:"https://fr.linkedin.com/in/benoitfauque"}],
				gallery : ["sw1", "sw2"]
			},{
				title : "2048",
				logo : '2048',
				tags : ['HTML/CSS', 'JavaScript', 'PHP', 'MySQL'],
				contentHeaders : [
					{label : "Choix du sujet", value : "Libre"},
					{label : "Contraintes", value : "Utilisation de fonctions web précises"},
					{label : "Délai", value : "1 mois"}
				],
				content : ["GALLERY",
				"Lors d'un cours sur le web à Polytech, nous devions proposer un site incluant certaines fonctionnalités particulières liées à l'utilisation des langages HTML, CSS, JavaScript et PHP. C'était l'occasion pour mon équipier et moi-même de re-créer le célèbre jeu du 2048. Le principe est simple : une grille de taille 4 par 4 contient des tuiles ayant toutes une valeur en puissance de 2. Lorsque le joueur appuie sur une touche directionnelle du clavier, elles se déplacent et celles dont les valeurs sont identiques fusionnent en formant une nouvelle tuile dont la puissance est doublée. Une nouvelle tuile contenant 2 ou 4 apparait à chaque déplacement. Ce site (en ligne à l'adresse ci-dessous) propose au joueur un classement en fin de partie, avec un tableau des scores pour répondre aux exigences en matière de PHP / MySQL. Ce projet a été réalisé avec l'aide de M. Gillot Mickaël. J'ai pour ma part principalement travaillé sur l'aspect serveur de l'application."],
				links : [
					{title:"Démonstration",link:"http://www.antoinefonfria.com/projects/2048"},
					{title:"Mickaël Gillot",link:"http://www.mickaelgillot.xyz"}],
				gallery : ["2048"]
			},{
				title : 'Planisphère interactif',
				logo : 'filipinos',
				tags : ['HTML/CSS', 'JavaScript', 'PHP', 'MySQL'],
				contentHeaders : [
					{label : "Choix du sujet", value : "Sélection dans une liste puis réservation"},
					{label : "Contraintes", value : "Adapté au tactile"},
					{label : "Délai", value : "2 x 3 mois"}
				],
				content : ["GALLERY",
				"Durant les deux années du parcours préparatoire de Polytech (2011-2013), nous devions réaliser nos premiers projets informatique. Après la première version de ce planisphère, M. Gougeon Nicolas et moi avons été invités à le poursuivre la seconde année, notre encadrant nous réservant le sujet. Cela consistait à développer une application permettant à un utilisateur de visualiser facilement les partenariats à l'étranger de l'école. La principale contrainte étant qu'il fallait la rendre compatible à un écran tactile géant. Nous avons donc créé un site web à l'ergonomie soigneusement pensée en ce sens. Utilisant la balise \"canvas\", la seconde version est assez aboutie, affichant une grande carte sur laquelle des points cliquables correspondent aux écoles à l'étranger. Elle comporte aussi une interface d'administration permettant de modifier la mappemonde, le titre et de placer les points enregistrés dans une base de données. Une démonstration est accessible à l'adresse ci-dessous."],
				links : [
					{title:"Démonstration (v2)", link:"http://www.antoinefonfria.com/projects/filipinos"},
					{title:"Article sur le blog de Polytech (v1)", link:"http://projets.polytech.univ-tours.fr/2012/05/12/planisphere-interactif/"},
					{title:"Nicolas Gougeon", link:"http://www.nicolasgougeon.com"}],
				gallery : ["filipinos0", "filipinos1", "filipinos2"]
			}
		]
	}
}
